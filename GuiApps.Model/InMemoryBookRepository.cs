﻿using System.Collections.Generic;
using System.Linq;

namespace GuiApps.Model
{
    public class InMemoryBookRepository : IBookRepository
    {
        private readonly List<Book> _books = new List<Book>();
        private int _currentIndex;

        public InMemoryBookRepository()
        {
            Init();
        }

        public IEnumerable<Book> SelectAll()
        {
            return new List<Book>(_books);
        }

        public Book Get(int id)
        {
            return _books.FirstOrDefault(x => x.Id == id);
        }

        public int Create(Book value)
        {
            var idx = ++_currentIndex;
            var entry = new Book(idx, value.Name, value.Author, value.Genre, value.Year);
            _books.Add(entry);
            return idx;
        }

        public void Update(Book value)
        {
            var entry = _books.FirstOrDefault(x => x.Id == value.Id);
            if (entry == null)
                return;
            entry.Name = value.Name;
            entry.Author = value.Author;
            entry.Genre = value.Genre;
            entry.Year = value.Year;
        }

        public void Delete(int id)
        {
            var entry = _books.FirstOrDefault(x => x.Id == id);
            if (entry == null)
                return;
            _books.Remove(entry);
        }

        public void Init()
        {
            Create(new Book("Мастер и Маргарита", "Михаил Булгаков", "Роман", 1929));
            Create(new Book("Преступление и наказание", "Федор Достоевский", "Роман", 1866));
            Create(new Book("Война и мир", "Лев Толстой", "Роман-эпопея", 1865));
            Create(new Book("Михаил Булгаков", "Собачье сердце", "Повесть", 1925));
            Create(new Book("Двенадцать стульев", "Илья Ильф, Евгений Петров", "Роман", 1928));
            Create(new Book("Мёртвые души", "Николай Гоголь", "Роман", 1842));
            Create(new Book("Братья Карамазовы", "Федор Достоевский", "Роман", 1879));
            Create(new Book("Идиот", "Федор Достоевский", "Роман", 1869));
            Create(new Book("Отцы и дети", "Иван Тургенев", "Роман", 1860));
            Create(new Book("Анна Каренина", "Лев Толстой", "Роман", 1873));
            Create(new Book("Золотой теленок", "Илья Ильф, Евгений Петров", "Роман", 1931));
            Create(new Book("Три товарища", "Эрих Мария Ремарк", "Роман", 1936));
            Create(new Book("Евгений Онегин", "Александр Пушкин", "Роман в стихах", 1825));
            Create(new Book("Горе от ума", "Александр Грибоедов", "Комедия", 1828));
        }
    }
}

﻿namespace GuiApps.Model
{
    public class Book
    {
        public int Id;
        public string Name;
        public string Author;
        public string Genre;
        public int Year;

        public Book(string name, string author, string genre, int year): this(0, name, author, genre, year)
        {
        }

        public Book(int id, string name, string author, string genre, int year)
        {
            Id = id;
            Name = name;
            Author = author;
            Genre = genre;
            Year = year;
        }
    }
}
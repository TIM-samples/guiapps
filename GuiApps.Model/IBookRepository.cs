﻿using System.Collections.Generic;

namespace GuiApps.Model
{
    public interface IBookRepository
    {
        void Init();
        IEnumerable<Book> SelectAll();
        Book Get(int id);
        int Create(Book value);
        void Update(Book value);
        void Delete(int id);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuiApps.Model;

namespace GuiApps.Web.Models
{
    public class BookViewModel
    {
        public int Id;
        public string Name;
        public string Author;
        public string Genre;
        public int Year;
    }

    public class Mapper
    {
        public static BookViewModel MapBook(Book book)
        {
            return new BookViewModel
            {
                Id = book.Id,
                Name = book.Name,
                Author = book.Author, 
                Genre = book.Genre, 
                Year = book.Year
            };
        }
    }
}

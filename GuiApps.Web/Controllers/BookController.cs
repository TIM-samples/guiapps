﻿using System;
using System.Linq;
using GuiApps.Model;
using GuiApps.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GuiApps.Web.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookRepository _repository;

        public BookController(IBookRepository repository)
        {
            _repository = repository;
        }

        // GET: Book
        public ActionResult Index()
        {
            var vm = _repository.SelectAll().Select(Mapper.MapBook);
            return View("List", vm);
        }

        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            var item = _repository.Get(id);
            var vm = Mapper.MapBook(item);
            return View(vm);
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            _repository.Create(new Book("<empty>", "<empty>", "<empty>", 0));
            return RedirectToAction(nameof(Index));
        }

        // GET: Book/Edit/5
        public ActionResult Edit(int id)
        {
            var item = _repository.Get(id);
            var vm = Mapper.MapBook(item);
            return View(vm);
        }

        // POST: Book/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                collection.TryGetValue(nameof(BookViewModel.Name), out var name);
                collection.TryGetValue(nameof(BookViewModel.Author), out var author);
                collection.TryGetValue(nameof(BookViewModel.Genre), out var genre);
                collection.TryGetValue(nameof(BookViewModel.Year), out var year);
                var item = new Book(id, name, author, genre, Convert.ToInt32(year));
                _repository.Update(item);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction(nameof(HomeController.Error), "Home");
            }
        }

        // GET: Book/Delete/5
        public ActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        // POST: Book/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                _repository.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction(nameof(HomeController.Error), "Home");
            }
        }
    }
}
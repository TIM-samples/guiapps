﻿namespace GuiApps.Wpf
{
    public class MainWindowViewModel : BaseViewModel
    {
        [Ninject.Inject]
        public LibViewModel LibViewModel { set; get; }

        public void LoadData()
        {
            LibViewModel.LoadData();
        }
    }
}
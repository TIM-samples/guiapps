﻿using System.Windows;
using GuiApps.Model;
using Ninject;

namespace GuiApps.Wpf
{
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            var container = new StandardKernel();
            container.Bind<IBookRepository>().To<InMemoryBookRepository>();
            var wnd = container.Get<MainWindow>();
            wnd.Show();
        }
    }
}

﻿namespace GuiApps.Wpf
{
    public class BookViewModel
    {
        public string Name { set; get; }

        public string Author { set; get; }

        public string Genre { set; get; }

        public int Year { set; get; }

        public BookViewModel(): this("<empty>", "<empty>", "<empty>", 0)
        {
        }

        public BookViewModel(string name, string author, string genre, int year)
        {
            Name = name;
            Author = author;
            Genre = genre;
            Year = year;
        }
    }
}
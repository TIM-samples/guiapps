﻿using System.Windows;

namespace GuiApps.Wpf
{
    public partial class MainWindow : Window
    {
        private readonly MainWindowViewModel _vm;

        public MainWindow(MainWindowViewModel vm)
        {
            InitializeComponent();
            _vm = vm;
            DataContext = _vm;
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            _vm.LoadData();
        }
    }
}

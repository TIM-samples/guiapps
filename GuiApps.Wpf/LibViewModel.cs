﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GuiApps.Model;

namespace GuiApps.Wpf
{
    public class LibViewModel : BaseViewModel
    {
        private readonly IBookRepository _repository;
        private readonly DialogService _dialogService;
        private ObservableCollection<BookViewModel> _books;

        public ObservableCollection<BookViewModel> Books
        {
            set { _books = value; OnPropertyChanged(nameof(Books)); }
            get => _books;
        }

        public ICommand AddCommand { private set; get; }
        public ICommand RemoveCommand { private set; get; }

        public LibViewModel(IBookRepository repository, DialogService dialogService)
        {
            _repository = repository;
            _dialogService = dialogService;
            Books = new ObservableCollection<BookViewModel>();
            AddCommand = new DelegateCommand(AddAction);
            RemoveCommand = new DelegateCommand(RemoveAction);
        }

        private void AddAction(object obj)
        {
            Books.Add(new BookViewModel());
        }

        private void RemoveAction(object obj)
        {
            try
            {
                if (obj == null)
                    throw new ArgumentException("Передано пустое значение (null)");

                var bookEntry = obj as BookViewModel;
                if (bookEntry == null)
                    throw new ArgumentException($"Тип объекта {nameof(obj)} не {nameof(BookViewModel)}");

                Books.Remove(bookEntry);
            }
            catch (Exception e)
            {
                _dialogService.NotifyError(e.Message);
            }
        }

        public void LoadData()
        {
            var data = _repository.SelectAll();
            Books.Clear();
            foreach (var book in data) 
                Books.Add(new BookViewModel(book.Name, book.Author, book.Genre, book.Year));
        }
    }
}

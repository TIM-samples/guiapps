﻿using System.Windows;

namespace GuiApps.Wpf
{
    public class DialogService
    {
        public void NotifyError(string message)
        {
            MessageBox.Show(message);
        }
    }
}
